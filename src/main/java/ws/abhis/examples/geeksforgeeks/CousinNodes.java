package ws.abhis.examples.geeksforgeeks;

/**
 * Created by abhishek on 8/27/14.
 * Question: Check if two nodes are cousin nodes. Two nodes are cousin nodes if they are at the same level but have different parents.
 */
public class CousinNodes {
    public boolean checkCousinNodes(BinaryTreeNode node1, BinaryTreeNode node2, BinaryTreeNode root) {
        //Check level of node1
        int levelNode1 = checkLevel(node1, root, 0);
        int levelNode2 = checkLevel(node2, root, 0);

        findParent(node1, root);
        BinaryTreeNode parent1 = binaryTreeNode;

        findParent(node2, root);
        BinaryTreeNode parent2 = binaryTreeNode;


        if ((levelNode1 == levelNode2) && (parent1 != parent2)) {
            return true;
        } else {
            return false;
        }

    }

    private BinaryTreeNode binaryTreeNode = new BinaryTreeNode();
    private void findParent(BinaryTreeNode node, BinaryTreeNode root) {

        if (root.getLeftNode() != null && root.getRightNode() != null) {
           findParent(node, root.getLeftNode());
           findParent(node, root.getRightNode());

        }



        if (root.getLeftNode() == node || root.getRightNode() == node) {
            binaryTreeNode = root;
        }



    }

    private int checkLevel(BinaryTreeNode node, BinaryTreeNode root, int tLevel) {
        tLevel++;
        if (root != node && root != null) {
            checkLevel(node, root.getLeftNode(), tLevel);
        }

        return ++tLevel;
    }
}
