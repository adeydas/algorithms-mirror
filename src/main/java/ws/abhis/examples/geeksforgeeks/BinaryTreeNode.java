package ws.abhis.examples.geeksforgeeks;

/**
 * Created by abhishek on 8/27/14.
 */
public class BinaryTreeNode {
    private int value;
    private BinaryTreeNode leftNode;

    public BinaryTreeNode getRightNode() {
        return rightNode;
    }

    public void setRightNode(BinaryTreeNode rightNode) {
        this.rightNode = rightNode;
    }

    public BinaryTreeNode getLeftNode() {
        return leftNode;
    }

    public void setLeftNode(BinaryTreeNode leftNode) {
        this.leftNode = leftNode;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    private BinaryTreeNode rightNode;
}
