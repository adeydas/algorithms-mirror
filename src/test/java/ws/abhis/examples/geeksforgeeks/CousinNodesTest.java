package ws.abhis.examples.geeksforgeeks;

import junit.framework.TestCase;

/**
 * Created by abhishek on 8/27/14.
 */
public class CousinNodesTest extends TestCase {
    public void testCheckCousinNodesFalse() {
        CousinNodes cousinNodes = new CousinNodes();


        //Build a tree
        BinaryTreeNode leaf1 = new BinaryTreeNode();
        leaf1.setValue(7);
        leaf1.setLeftNode(null);
        leaf1.setRightNode(null);

        BinaryTreeNode leaf2 = new BinaryTreeNode();
        leaf2.setValue(8);
        leaf2.setLeftNode(null);
        leaf2.setRightNode(null);

        BinaryTreeNode leaf3 = new BinaryTreeNode();
        leaf3.setValue(1);
        leaf3.setLeftNode(null);
        leaf3.setRightNode(null);

        BinaryTreeNode leaf4 = new BinaryTreeNode();
        leaf4.setValue(3);
        leaf4.setLeftNode(null);
        leaf4.setRightNode(null);

        BinaryTreeNode node3 = new BinaryTreeNode();
        node3.setValue(3);
        node3.setLeftNode(leaf1);
        node3.setRightNode(leaf2);

        BinaryTreeNode node5 = new BinaryTreeNode();
        node5.setValue(5);
        node5.setLeftNode(leaf3);
        node5.setRightNode(leaf4);



        BinaryTreeNode root = new BinaryTreeNode();
        root.setValue(6);
        root.setLeftNode(node3);
        root.setRightNode(node5);



        assertTrue(!cousinNodes.checkCousinNodes(node3, node5, root));
    }


    public void testCheckCousinNodesTrue() {
        CousinNodes cousinNodes = new CousinNodes();


        //Build a tree
        BinaryTreeNode leaf1 = new BinaryTreeNode();
        leaf1.setValue(7);
        leaf1.setLeftNode(null);
        leaf1.setRightNode(null);

        BinaryTreeNode leaf2 = new BinaryTreeNode();
        leaf2.setValue(8);
        leaf2.setLeftNode(null);
        leaf2.setRightNode(null);

        BinaryTreeNode leaf3 = new BinaryTreeNode();
        leaf3.setValue(1);
        leaf3.setLeftNode(null);
        leaf3.setRightNode(null);

        BinaryTreeNode leaf4 = new BinaryTreeNode();
        leaf4.setValue(3);
        leaf4.setLeftNode(null);
        leaf4.setRightNode(null);

        BinaryTreeNode node3 = new BinaryTreeNode();
        node3.setValue(3);
        node3.setLeftNode(leaf1);
        node3.setRightNode(leaf2);

        BinaryTreeNode node5 = new BinaryTreeNode();
        node5.setValue(5);
        node5.setLeftNode(leaf3);
        node5.setRightNode(leaf4);

        BinaryTreeNode root = new BinaryTreeNode();
        root.setValue(6);
        root.setLeftNode(node3);
        root.setRightNode(node5);



        assertTrue(cousinNodes.checkCousinNodes(leaf1, leaf4, root));
    }
}
